# Service Giter8 Template

This repository contains a template for new services backend.

## How to use

Initialize template from branch `main`
```shell
sbt new https://gitlab.com/romer533/giter8template.g8.git
```
If you need to initialize a template from another branch, you need to use the key `-b`.

More details can be found on the website: http://www.foundweekends.org/giter8/index.html

## Template Options

`name` - service name and project directory
