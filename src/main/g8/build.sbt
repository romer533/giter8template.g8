import Dependencies._

val jfrogCredentials = Credentials("Artifactory Realm", "gdl.jfrog.io", System.getenv("jfrogUser"), System.getenv("jfrogPassword"))
val projectResolvers = Seq(
  Resolver.sonatypeRepo("snapshots"),
  "Artifactory".at(s"https://gdl.jfrog.io/gdl/gdlcommon/")
)

lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .configs(IntegrationTest)
  .settings(
    scalaVersion := Versions.scala,
    organization := "com.glidewell",
    name         := "$name$",
    scalacOptions ++= Seq(
      "-Xfatal-warnings",
      "-Wunused:imports,privates,locals,patvars",
      "-Wconf:src=src_managed/.*:silent",
      "-Wdead-code",
      "-Xlint:infer-any",
      "-feature",
      "-language:existentials"
    ),
    credentials += jfrogCredentials,
    resolvers ++= projectResolvers,
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision,
    testFrameworks += new TestFramework("weaver.framework.CatsEffect"),
    IntegrationTest / fork := true,
    Defaults.itSettings,
    ivyConfigurations += Datadog.DatadogConfig,
    Universal / mappings += Datadog.findDatadogJavaAgent(update.value) -> "datadog/dd-java-agent.jar",
    bashScriptExtraDefines ++= Datadog.datadogSetup.value,
    libraryDependencies ++= Dependencies.core ++ Dependencies.test,
    scalafixOnCompile := true,
    scalafmtOnCompile := true
  )
