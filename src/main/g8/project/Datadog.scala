import sbt.Keys.{name, version}
import sbt._

import java.io.File

object Datadog {

  val DatadogConfig = config("datadog-agent").hide
  val datadogFilter = artifactFilter(name = "dd-java-agent*", `type` = "jar")

  def findDatadogJavaAgent(report: UpdateReport): File = {
    val files = report.matching(datadogFilter)

    files.foreach(f => println(s"[DataDogFilter] Found artifact: \${f.getName}"))
    files.head
  }

  val datadogSetup = Def.setting(
    Seq(
      """addJava "-javaagent:\${app_home}/../datadog/dd-java-agent.jar"""",
      s"""addJava "-Ddd.service.name=\$name"""",
      s"""addJava "-Ddd.service.mapping=java-aws-sdk:\$name-aws-sdk"""",
      s"""addJava "-Ddd.agent.port=8126"""",
      """addJava "-Ddd.agent.host=\$(curl http://169.254.169.254/latest/meta-data/local-ipv4 -m 5 || echo 'unknown')"""",
      s"""addJava "-Ddd.version=\${version.value}"""",
      """export DD_API_KEY=1ba085c2e5a3efdac1cea16d330567ef""", //it seems that java system property for API Key is simply ignored by DD
      """export DD_TRACE_ENABLED=false""",                      //turn off tracing. To turn on, add `-Ddd.trace.enabled=true` to cmd args.
      """export DD_PROFILING_ENABLED=false""" //turn off profiling (just for safety, default is already 'false'). To turn on, add `-Ddd.profiling.enabled=true` to cmd args.
    )
  )

}
