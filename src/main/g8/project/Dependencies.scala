import Datadog.DatadogConfig
import sbt._

object Dependencies {

  object Versions {
    val scala = "2.13.8"

    val cats       = "2.8.0"
    val catsEffect = "3.3.14"
    val catsMtl    = "1.3.0"
    val log4cats   = "2.4.0"
    val logback    = "1.2.11"
    val pureConfig = "0.17.1"
    val tapir      = "0.19.3"
    val r7         = "3.0.15"
    val datadog    = "0.80.0"

    val testcontainers = "1.16.2"
    val weaver         = "0.7.15"
  }

  val catsCore        = "org.typelevel"               %% "cats-core"           % Versions.cats
  val catsEffect      = "org.typelevel"               %% "cats-effect"         % Versions.catsEffect
  val catsFree        = "org.typelevel"               %% "cats-free"           % Versions.cats
  val catsMtl         = "org.typelevel"               %% "cats-mtl"            % Versions.catsMtl
  val log4catsCore    = "org.typelevel"               %% "log4cats-core"       % Versions.log4cats
  val log4catsSlf4f   = "org.typelevel"               %% "log4cats-slf4j"      % Versions.log4cats
  val logback         = "ch.qos.logback"               % "logback-classic"     % Versions.logback
  val pureConfig      = "com.github.pureconfig"       %% "pureconfig"          % Versions.pureConfig
  val tapirHttp4s     = "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % Versions.tapir
  val tapirSttpClient = "com.softwaremill.sttp.tapir" %% "tapir-sttp-client"   % Versions.tapir
  val datadog         = "com.datadoghq"                % "dd-java-agent"       % Versions.datadog % DatadogConfig
  val r7              = "com.rapid7"                   % "r7insight_java"      % Versions.r7

  val testcontainers = "org.testcontainers"   % "testcontainers" % Versions.testcontainers
  val weaver         = "com.disneystreaming" %% "weaver-cats"    % Versions.weaver

  val core: Seq[ModuleID] =
    Seq(
      catsCore,
      catsEffect,
      catsFree,
      catsMtl,
      log4catsCore,
      log4catsSlf4f,
      logback,
      pureConfig,
      tapirHttp4s,
      tapirSttpClient,
      datadog,
      r7
    )

  val test: Seq[ModuleID] =
    Seq(
      testcontainers,
      weaver
    ).map(_ % "it,test")
}
