package com.glidewell

import cats.effect.IO
import cats.effect.Resource

object TestSpec extends TestUnit {

  override type Res = Unit

  override def sharedResource: Resource[IO, Res] = Resource.unit

  test("Naive test") { _ =>
    IO(expect.same("True", "True"))
  }

}
