package com.glidewell

import weaver._

trait TestUnit extends IOSuite {
  
  override def maxParallelism: Int = 1

}
