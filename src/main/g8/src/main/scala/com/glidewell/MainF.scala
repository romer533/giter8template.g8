package com.glidewell

import cats.effect.{Async, Resource, Sync}
import cats.syntax.flatMap._
import com.glidewell.config.PureConfig
import org.typelevel.log4cats.Logger

trait MainF {

  def runProgram[F[_]: Async: Logger]: F[Unit] = {
    val definition = for {
      config <- PureConfig.getFullResource[F]
      _      <- Resource.eval(Logger[F].info(s"Hello \${config.server.host}:\${config.server.port}"))
    } yield ()

    Sync[F].handleErrorWith(definition.use(_ => Sync[F].never[Unit])) { e =>
      Logger[F].error(s"Any error \$e").flatTap(_ => Sync[F].delay(e.printStackTrace()))
    }
  }

}
