package com.glidewell.config

import cats.effect.{Resource, Sync}
import pureconfig.ConfigSource
import pureconfig.generic.auto._

import scala.util.Try

object PureConfig {

  def getFullResource[F[_]: Sync]: Resource[F, Configs] =
    Resource.eval {
      Sync[F].fromTry(Try(ConfigSource.default.loadOrThrow[Configs]))
    }

}
